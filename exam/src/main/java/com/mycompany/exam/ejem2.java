
package com.mycompany.exam;

/**
 *
 * @author Piter Rolando Argandoña Aramayo      CI 1271740
 */
public class ejem2 {
   char arr[];
    int top;
    
    ejem2(){
       this.arr = new char [6];
       this.top = -1;
    }
    public boolean isEmpty(){
        return(top == -1);
    }
    
    public void push (char elemento){
        top++;
        if (top < arr.length){
            arr[top] =elemento;
        }
        else{
             char temp[] = new char[arr.length+5];
            for(int i = 0; i<arr.length; i++){
                temp[i] = arr[i];
            }
            
            arr = temp;
            arr[top] = elemento;
        }
    }
    
    public char peek(){
        return arr[top];
    }
    
    public char pop(){
        if(!isEmpty()){
            int mat = top;
            top--;
            char tan = arr[mat];
            return tan;
        }
        else{
            return '-';
        }
    }
}
